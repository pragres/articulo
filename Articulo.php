<?php

class Articulo extends Service
{
	/**
	 * Load the Service when an email request arrives
	 * 
	 * @param Request
	 * */
	public function main($request){
		$from = $request->requestor->email;
		$argument = $request->argument;
		$body = $request->body;

		// clean the argument
		$argument = str_replace("\n", " ", $argument);
		$argument = str_replace("\r", "", $argument);
		$argument = trim($argument);

		// trying to math the subject as it come to get a response
		$keyword = urlencode($argument);
		$article = $this->wiki_get($keyword);

		// if no article was found with exact math, find similar entries in wikipedia
		if( ! $article)
		{
			$wikisearch = $this->wiki_search($keyword);
			if(sizeof($wikisearch) > 0)
			{
				$keyword = urlencode($wikisearch[0]);
				$article = $this->wiki_get($keyword);
			}
		}

		// if the articule was found, send the information to the template
		if($article)
		{
			// create a json object to send to the template
			$json = '{
				"showimages": "'.$article['showimages'].'",
				"title": "'.$article['title'].'",
				"article_body": "'.$this->cleanStringForJSON($article['article_body']).'"
			}';

			// configure the response object to send to the template
			$this->response->createUserDefinedResponse("basic.tpl", $json);

			// attach all images to the response object
			foreach($article['images'] as $image)
			{
				$attachment = new Attachment($image['type'], base64_encode($image['content']), $image['name'], $image['id']);
				$this->response->addAttachment($attachment);
			}
		}
		else // if no article was found at all, tell the user
		{
			$text = "Lo siento, pero no se encontramos ningun art&iacute;culo con titulo: $argument. Intente nuevamente con una frase similar.";
			$this->response->createTextResponse($text);
		}
	}

	/**
	 * Clean a string so it can be used inside a JSON
	 *
	 * @author salvipascual
	 * @param String $text
	 * @return String
	 * */
	private function cleanStringForJSON($text){
		return str_replace('"', "'", str_replace("'", "`", preg_replace("/\s+/", " ", $text)));
	}

	/**
	 * Get article from wikipedia
	 *
	 * @author rafa
	 * @author salvipascual
	 * @param string $keyword Phrase to search for in wikipedia
	 * @return array
	 */
	private function wiki_get($keyword){
		$completo = false;
		$url = "http://es.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=xml&redirects=1&titles=$keyword&rvparse";
		$page = file_get_contents($url);

		if (strpos($page, 'missing=""') === false)
		{
			if ($this->utils->isUTF8($page)) $page = utf8_decode($page);
			$page = html_entity_decode($page, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1');
	
			// ---------------------
			$p1 = strpos($page, '<page pageid=');
			$p2 = strpos($page, '<revisions');
			$title = substr($page, $p1, $p2 - $p1);

			// 1 article found in WikiQuery
			$p1 = strpos($title, 'title="');
			$p2 = strpos($title, '">', $p1);
			$title = substr($title, $p1 + 7, $p2 - $p1 - 7);
	
			// remove everything before the index and external links
			$mark = '<rev xml:space="preserve">';
			$page = substr($page, strpos($page, $mark) + strlen($mark));
			$page = str_replace('</rev></revisions></page></pages></query></api>', '', $page);

			//Striping tags
			$page = strip_tags($page, '<a><!--><!DOCTYPE><abbr><acronym><address><area><article><aside><b><base><basefont><bdi><bdo><big><blockquote><body><br><button><canvas><caption><center><cite><code><col><colgroup><command><datalist><dd><del><details><dfn><dialog><dir><div><dl><dt><em><embed><fieldset><figcaption><figure><font><footer><form><frame><frameset><head><header><h1> - <h6><hr><html><i><iframe><img><input><ins><kbd><keygen><label><legend><li><link><map><mark><menu><meta><meter><nav><noframes><noscript><object><ol><optgroup><option><output><p><param><pre><progress><q><rp><rt><ruby><s><samp><script><section><select><small><source><span><strike><strong><style><sub><summary><sup><table><tbody><td><textarea><tfoot><th><thead><time><title><tr><track><tt><u><ul><var><wbr><h2><h3>');
			$page = str_replace('oding="UTF-8"?>', '', $page);
	
			// removing brackets []
			$page = preg_replace('/\[([^\[\]]++|(?R))*+\]/', '', $page);
	
			// remove indice
			$hpage = $page; // html_entity_decode($page);
	
			$mark = '<div id="toc" class="toc">';
	
			$p1 = strpos($hpage, $mark);
			if ($p1 !== false) {
				$p2 = strpos($hpage, '</div>', $p1);
				if ($p2 !== false) {
					$p2 = strpos($hpage, '</div>', $p2 + 1);
					$hpage = substr($hpage, 0, $p1) . substr($hpage, $p2 + 6);
				}
			}
	
			// remove enlaces externos
			$mark = '<span class="mw-headline" id="Enlaces_externos';
			$p = strpos($hpage, $mark);
			if ($p !== false)
				$hpage = substr($hpage, 0, $p - 4);
				
			// remove other stuff
			$hpage = str_replace("</api>", "", $hpage);
			$hpage = str_replace("<api>", "", $hpage);
	
			// remove references links
			$p = strpos($hpage, '<h2><span class="mw-headline" id="Referencias">');
			if ($p !== false) {
				$part = substr($hpage, $p);
				$part = strip_tags($part, '<li><ul><span><h2><h3>');
				$hpage = substr($hpage, 0, $p) . $part;
			}
	
			$hpage = str_replace('>?</span>', '></span>', $hpage);
	
			$page = trim($hpage);
	
			if ($page != '') {
					
				// Build our DOMDocument, and load our HTML
				$doc = new DOMDocument();
				@$doc->loadHTML($page);

				// New-up an instance of our DOMXPath class
				$xpath = new DOMXPath($doc);

				// Find all elements whose class attribute has test2
				$elements = $xpath->query("//*[contains(@class,'thumb')]");

				// Cycle over each, remove attribute 'class'
				foreach ( $elements as $element )
				{
					// Empty out the class attribute value
					$element->parentNode->removeChild($element);
				}
					
				// Load images
				$imagestags = $doc->getElementsByTagName("img");
				$images = array();
				$ignored = array();
				if ($imagestags->length > 0) {
					foreach ( $imagestags as $imgtag ) {
							
						$imgsrc = $imgtag->getAttribute('src');
							
						if (substr($imgsrc, 0, 2) == '//')
							$imgsrc = 'http:' . $imgsrc;
							
						if (stripos($imgsrc, 'increase') !== false || stripos($imgsrc, 'check') !== false || stripos($imgsrc, 'mark') !== false || stripos($imgsrc, 'emblem') !== false || stripos($imgsrc, 'symbol_comment') !== false || substr($imgsrc, - 4) == ".svg") {
							$ignored[] = $imgsrc;
							continue;
						}

						$srcparts = explode("/", $imgsrc);
						$name = array_pop($srcparts);

						//Retrieving image $imgsrc
						$img = @file_get_contents($imgsrc);
						if ($img === false) continue;

						// Compressing image $imgsrc
						$img = base64_decode($this->utils->resizeImage(base64_encode($img), 100));

						if ("$img" != "") {
							$ext = substr($imgsrc, - 3);
							$id = uniqid();

							$images[] = array(
								"type" => "image/$ext",
								"content" => $img,
								"name" => $name,
								"id" => $id,
								"src" => str_replace("http:", "", $imgsrc)
							);
						}
					}
				}
					
				// Output the HTML of our container
				$page = $doc->saveHTML();
					
				// Cleanning
				$page = str_replace("<br>", "<br>\n", $page);
				$page = str_replace("<br/>", "<br/>\n", $page);
				$page = str_replace("</p>", "</p>\n", $page);
				$page = str_replace("</h2>", "</h2>\n", $page);
				$page = str_replace("</span>", "</span>\n", $page);
				$page = str_replace("/>", "/>\n", $page);
				$page = str_replace("<p", "<p style=\"text-align:justify;\" align=\"justify\"", $page);
				$page = wordwrap($page, 200, "\n");
				$page = str_replace("href=\"/wiki/", 'href="mailto:{$_SERVICE_EMAIL}?subject=ARTICULO  ', $page);
					
				foreach ( $images as $image ) {
					$page = str_replace($image['src'], "cid:" . $image['id'], $page);
				}
				foreach ( $ignored as $ign ) {
					$page = str_replace($ign, '', $page);
				}
					
				$showimages = true;
				if ( ! $completo)
				{
					$size = strlen($page);
					foreach ($images as $image) $size += sizeof($image['content']);
	
					if ($size > 1024 * 450)
					{
						$images = array();
						$showimages = false;
						$page = strip_tags($page, '<a><abbr><acronym><address><applet><area><article><aside><audio><b><base><basefont><bdi><bdo><big><blockquote><br><button><canvas><caption><center><cite><code><col><colgroup><command><datalist><dd><del><details><dfn><dialog><dir><div><dl><dt><em><embed><fieldset><figcaption><figure><font><footer><form><frame><frameset><head><header><h1> - <h6><hr><i><iframe><input><ins><kbd><keygen><label><legend><li><link><map><mark><menu><meta><meter><nav><noframes><noscript><object><ol><optgroup><option><output><p><param><pre><progress><q><rp><rt><ruby><s><samp><script><section><select><small><source><span><strike><strong><style><sub><summary><sup><table><tbody><td><textarea><tfoot><th><thead><time><title><tr><track><tt><u><ul><var><video><wbr><h2><h3>');
					}
				}

				// save content into pages that will go to the view
				return array(
					"title" => "$title",
					"article_body" => $page,
					"showimages" => $showimages,
					"images" => $images,
					"size" => intval($size / 1024)
				);
			}
		}

		return false;
	}
	
	/**
	 * Search articles in wikipedia with OpenSearch
	 *
	 * @author rafa
	 * @author salvipascual
	 * @param String $query Phrase to search for
	 * @return array
	 */
	private function wiki_search($keyword){
		// Triying OpenSearch
		$url = "http://es.wikipedia.org/w/api.php?action=opensearch&search=$keyword&limit=10&namespace=0&format=json";
		$page = file_get_contents($url);
		$data = json_decode($page);

		// OpenSearch result: $page
		$results = $data[1];

		if ( ! isset($results[0])) return false;
		else return $results;
	}
}
